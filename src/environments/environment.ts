// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAGOT6ozG8LkZ5KgIAcOOlN7KS3aOgcS-4',
    authDomain: 'calculate-my-bmi.firebaseapp.com',
    projectId: 'calculate-my-bmi',
    storageBucket: 'calculate-my-bmi.appspot.com',
    messagingSenderId: '832881203842',
    appId: '1:832881203842:web:5b9b577e1c10271088729b',
    measurementId: 'G-3F1C71QB7H',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
