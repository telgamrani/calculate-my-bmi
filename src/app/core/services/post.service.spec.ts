import { TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { environment } from 'src/environments/environment';

import { PostService } from './post.service';

describe('PostService', () => {
  let service: PostService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule,
      ],
      providers: [PostService],
    });

    service = TestBed.get(PostService);
  });

  it('create service', () => {
    expect(service).toBeTruthy();
  });

  it('fetch post by id', (done: DoneFn) => {
    const postId = 'post-2';
    service.fetchPost(postId).subscribe((post) => {
      expect(post).withContext('post exists').toBeTruthy();
      expect(post?.title).withContext('title exists').toBeTruthy();
      expect(post?.title.length)
        .withContext('title length > 0')
        .toBeGreaterThan(0);
      done();
    });
  });

  it('fetch all posts', (done: DoneFn) => {
    service.fetchAllPosts().subscribe((posts) => {
      expect(posts).withContext('posts exists').toBeTruthy();
      expect(posts.length).withContext('posts length > 0').toBeGreaterThan(0);
      done();
    });
  });
});
