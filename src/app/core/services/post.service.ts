import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';
import { map, Observable, of } from 'rxjs';
import { Post } from 'src/app/shared/models/post.model';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  private dbPath = 'posts';
  postsCollection: AngularFirestoreCollection<Post>;

  constructor(private db: AngularFirestore) {
    this.postsCollection = this.db.collection(this.dbPath);
  }

  fetchAllPosts(): Observable<Array<Post>> {
    return this.postsCollection.snapshotChanges().pipe(
      map((posts) =>
        posts.map((post) => ({
          ...post.payload.doc.data(),
          id: post.payload.doc.id,
        }))
      )
    );
  }

  fetchPost(postId: string): Observable<Post | undefined> {
    return this.postsCollection
      .doc(postId)
      .snapshotChanges()
      .pipe(
        map((post) => {
          return post.payload.data();
        })
      );
  }
}
