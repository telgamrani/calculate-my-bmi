import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicComponentLoaderHostDirective } from './dynamic-component-loader-host.directive';

@NgModule({
  declarations: [DynamicComponentLoaderHostDirective],
  imports: [CommonModule],
  exports: [DynamicComponentLoaderHostDirective],
})
export class DynamicComponentLoaderHostModule {}
