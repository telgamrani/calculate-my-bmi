import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[cmbDynamicComponentLoaderHost]',
})
export class DynamicComponentLoaderHostDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
