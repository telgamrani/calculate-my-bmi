import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  it('create component', () => {
    expect(component).toBeTruthy();
  });

  it('find nav html element', () => {
    expect(debugElement.query(By.css('nav')))
      .withContext('nav element')
      .toBeTruthy();
  });

  it('find home html element', () => {
    expect(debugElement.query(By.css('[data-test-home-link-element]')))
      .withContext('home element')
      .toBeTruthy();
  });

  it('find blog html element', () => {
    expect(debugElement.query(By.css('[data-test-blog-link-element]')))
      .withContext('blog element')
      .toBeTruthy();
  });
});
