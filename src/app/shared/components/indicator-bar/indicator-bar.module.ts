import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndicatorBarComponent } from './indicator-bar/indicator-bar.component';

@NgModule({
  declarations: [IndicatorBarComponent],
  imports: [CommonModule],
  exports: [IndicatorBarComponent],
})
export class IndicatorBarModule {}
