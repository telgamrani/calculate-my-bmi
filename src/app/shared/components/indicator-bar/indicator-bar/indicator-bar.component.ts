import { Component, Input } from '@angular/core';

@Component({
  selector: 'cmb-indicator-bar',
  templateUrl: './indicator-bar.component.html',
  styleUrls: ['./indicator-bar.component.scss'],
})
export class IndicatorBarComponent {
  @Input() indicatorValue: number;
}
