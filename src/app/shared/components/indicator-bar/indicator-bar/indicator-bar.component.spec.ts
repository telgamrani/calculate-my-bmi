import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { IndicatorBarComponent } from './indicator-bar.component';

describe('IndicatorBarComponent', () => {
  let component: IndicatorBarComponent;
  let fixture: ComponentFixture<IndicatorBarComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IndicatorBarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(IndicatorBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  it('create compponent', () => {
    expect(component).toBeTruthy();
  });

  it('have blue bar with a width === 18.5%', () => {
    const barBlue = debugElement.query(By.css('.blue'));
    expect(barBlue).withContext('barBlue').toBeTruthy();
  });

  it('have gree bar with a width === 6.5%', () => {
    const barGreen = debugElement.query(By.css('.green'));
    expect(barGreen).withContext('barGreen').toBeTruthy();
  });

  it('have yellow bar with a width === 5%', () => {
    const barYellow = debugElement.query(By.css('.yellow'));
    expect(barYellow).withContext('barYellow').toBeTruthy();
  });

  it('have red bar with a width === 70%', () => {
    const barRed = debugElement.query(By.css('.red'));
    expect(barRed).withContext('barRed').toBeTruthy();
  });

  it('have blue value with a width === 18.5%', () => {
    const valueBlue = debugElement.query(By.css('.value-blue'));
    expect(valueBlue).withContext('valueBlue').toBeTruthy();
  });

  it('have gree value with a width === 6.5%', () => {
    const valueGreen = debugElement.query(By.css('.value-green'));
    expect(valueGreen).withContext('valueGreen').toBeTruthy();
  });

  it('have yellow value with a width === 5%', () => {
    const valueYellow = debugElement.query(By.css('.value-yellow'));
    expect(valueYellow).withContext('valueYellow').toBeTruthy();
  });

  it('have red value with a width === 70%', () => {
    const valueRed = debugElement.query(By.css('.value-red'));
    expect(valueRed).withContext('valueRed').toBeTruthy();
  });

  it('have indicator element with a width === input value', () => {
    const indicatorElement = debugElement.query(By.css('.indicator'));
    expect(indicatorElement).withContext('indicatorElement').toBeTruthy();
  });

  it('have indicator value element with a value content === input value', () => {
    component.indicatorValue = 24.5;
    fixture.detectChanges();
    const indicatorValueElement = debugElement.query(
      By.css('.indicator-value')
    );
    expect(indicatorValueElement)
      .withContext('indicatorValueElement')
      .toBeTruthy();
    expect(indicatorValueElement.nativeElement.textContent)
      .withContext('indicatorValueElement content value')
      .toBe(component.indicatorValue + '');
  });
});
