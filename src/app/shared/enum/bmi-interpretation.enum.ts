export enum BmiInterpretation {
  Underweight,
  NormalWeight,
  Overweight,
  Obesity,
}
