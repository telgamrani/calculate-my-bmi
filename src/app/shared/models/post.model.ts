interface UniversalTime {
  nanoseconds: number;
  seconds: number;
}

export interface Post {
  id: string;
  title: string;
  img: string;
  content: string;
  dateCreation: UniversalTime;
  dateStartPublication: UniversalTime;
}
