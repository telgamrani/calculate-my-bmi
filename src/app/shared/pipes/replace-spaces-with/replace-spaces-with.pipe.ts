import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceSpacesWith',
})
export class ReplaceSpacesWithPipe implements PipeTransform {
  transform(pattern: string, replacement: string): unknown {
    return pattern?.replace(/[,:]/g, '').replace(/(\'|\s+)/g, replacement);
  }
}
