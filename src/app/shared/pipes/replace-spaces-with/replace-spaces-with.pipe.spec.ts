import { ReplaceSpacesWithPipe } from './replace-spaces-with.pipe';

describe('ReplaceSpacesWithPipe', () => {
  const pipe = new ReplaceSpacesWithPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('replace spaces with the given string parameter', () => {
    expect(pipe.transform('text to test', '-')).toBe('text-to-test');
  });
});
