import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReplaceSpacesWithPipe } from './replace-spaces-with.pipe';

@NgModule({
  declarations: [ReplaceSpacesWithPipe],
  imports: [CommonModule],
  exports: [ReplaceSpacesWithPipe],
})
export class ReplaceSpacesWithModule {}
