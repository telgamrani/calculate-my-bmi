import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogPostPageComponent } from './blog-post-page/blog-post-page.component';
import { CalculateMyBmiFormModule } from '../calculate-my-bmi-form/calculate-my-bmi-form.module';
import { SanitizeHtmlPipe } from '../shared/pipes/sanitize-html.pipe';
import { RouterModule } from '@angular/router';
import { ReplaceSpacesWithModule } from '../shared/pipes/replace-spaces-with/replace-spaces-with.module';
import { DynamicComponentLoaderHostModule } from '../shared/directives/dynamic-component-loader-host/dynamic-component-loader-host.module';

@NgModule({
  declarations: [BlogPostPageComponent, SanitizeHtmlPipe],
  imports: [
    CommonModule,
    CalculateMyBmiFormModule,
    ReplaceSpacesWithModule,
    RouterModule,
    DynamicComponentLoaderHostModule,
  ],
  exports: [BlogPostPageComponent],
})
export class BlogPostPageModule {}
