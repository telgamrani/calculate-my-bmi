import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { By } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';

import { BlogPostPageComponent } from './blog-post-page.component';

describe('BlogPostPageComponent', () => {
  let component: BlogPostPageComponent;
  let fixture: ComponentFixture<BlogPostPageComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BlogPostPageComponent],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule,
        RouterModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BlogPostPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  xit('create component', () => {
    expect(component).toBeTruthy();
  });

  xit('have main html element', () => {
    expect(debugElement.query(By.css('main')))
      .withContext('main element')
      .toBeTruthy();
  });

  xit('have article html element', () => {
    expect(debugElement.query(By.css('article')))
      .withContext('article element')
      .toBeTruthy();
  });

  xit('have only one h1 html element', () => {
    const h1Elements = debugElement.query(By.css('h1'));
    expect(h1Elements?.childNodes?.length).withContext('h1Elements').toBe(1);
  });

  xit('have internal links', () => {
    expect(debugElement.query(By.css('[data-test-internal-links]')))
      .withContext('internal links')
      .toBeTruthy();
  });
});
