import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { CalculateMyBmiFormComponent } from 'src/app/calculate-my-bmi-form/calculate-my-bmi-form/calculate-my-bmi-form.component';
import { PostService } from 'src/app/core/services/post.service';
import { DynamicComponentLoaderHostDirective } from 'src/app/shared/directives/dynamic-component-loader-host/dynamic-component-loader-host.directive';
import { Post } from 'src/app/shared/models/post.model';

@Component({
  selector: 'cmb-blog-post-page',
  templateUrl: './blog-post-page.component.html',
  styleUrls: ['./blog-post-page.component.scss'],
})
export class BlogPostPageComponent implements OnInit {
  @ViewChild(DynamicComponentLoaderHostDirective, { static: true })
  dynamicComponentLoaderHostDirective!: DynamicComponentLoaderHostDirective;

  post: Post | undefined;
  postsForInternalLinks: Array<Post>;
  //data-placholder-to-inject-calculate-bmi-component
  constructor(
    private postService: PostService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.fetchPost();
    this.fetchAllPosts();
  }

  fetchPost() {
    this.route.params
      .pipe(switchMap((response) => this.postService.fetchPost(response['id'])))
      .subscribe((response) => (this.post = response));
  }

  fetchAllPosts() {
    this.postService.fetchAllPosts().subscribe((posts: Array<Post>) => {
      this.postsForInternalLinks = posts;
      this.loadCalculateMyBmiFormComponent();
    });
  }

  private loadCalculateMyBmiFormComponent() {
    const viewContainerRef =
      this.dynamicComponentLoaderHostDirective.viewContainerRef;
    viewContainerRef.clear();
    viewContainerRef.createComponent<CalculateMyBmiFormComponent>(
      CalculateMyBmiFormComponent
    );
    this.deplaceCalculateMyBmiForm();
  }

  private deplaceCalculateMyBmiForm() {
    const calculateMyBmiForm = document.getElementById('calculateMyBmiForm');
    const sections = document.getElementsByTagName('section');

    if (calculateMyBmiForm && sections && sections.length > 2) {
      const firstSection = sections.item(1);
      const sectionCalculateMyBmiForm = document.createElement('section');
      sectionCalculateMyBmiForm.innerHTML = '<h2>Title</h2><p>description</p>';
      sectionCalculateMyBmiForm.appendChild(calculateMyBmiForm);

      (firstSection?.parentNode as Node).insertBefore(
        sectionCalculateMyBmiForm,
        firstSection
      );
    }
  }
}
