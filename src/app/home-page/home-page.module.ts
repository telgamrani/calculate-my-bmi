import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { CalculateMyBmiFormModule } from '../calculate-my-bmi-form/calculate-my-bmi-form.module';
import { RouterModule } from '@angular/router';
import { ReplaceSpacesWithModule } from '../shared/pipes/replace-spaces-with/replace-spaces-with.module';

@NgModule({
  declarations: [HomePageComponent],
  imports: [
    CommonModule,
    CalculateMyBmiFormModule,
    ReplaceSpacesWithModule,
    RouterModule,
  ],
  exports: [HomePageComponent],
})
export class HomePageModule {}
