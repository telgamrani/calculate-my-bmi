import { DebugElement } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { By } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { PostService } from 'src/app/core/services/post.service';
import { environment } from 'src/environments/environment';

import { HomePageComponent } from './home-page.component';

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomePageComponent],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule,
        RouterModule,
      ],
      providers: [PostService],
    }).compileComponents();

    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  it('create component', () => {
    expect(component).toBeTruthy();
  });

  it('have main html element', () => {
    expect(debugElement.query(By.css('main')))
      .withContext('main element')
      .toBeTruthy();
  });

  it('have article html element', () => {
    expect(debugElement.query(By.css('article')))
      .withContext('article element')
      .toBeTruthy();
  });

  it('have only one h1 html element', () => {
    const h1Elements = debugElement.query(By.css('h1'));
    expect(h1Elements?.childNodes?.length).withContext('h1Elements').toBe(1);
  });

  xit('have internal links', fakeAsync(() => {
    // TODO : impl asyn test
    component.ngOnInit();
    tick(10000);
    fixture.detectChanges();
    expect(debugElement.query(By.css('[data-test-internal-links]')))
      .withContext('internal links')
      .toBeTruthy();
  }));
});
