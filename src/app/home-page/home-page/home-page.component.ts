import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/core/services/post.service';
import { Post } from 'src/app/shared/models/post.model';

@Component({
  selector: 'cmb-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  posts: Array<Post>;

  constructor(private postService: PostService) {}

  ngOnInit(): void {
    this.fetchAllPosts();
  }

  fetchAllPosts() {
    this.postService.fetchAllPosts().subscribe((posts: Array<Post>) => {
      this.posts = posts;
    });
  }
}
