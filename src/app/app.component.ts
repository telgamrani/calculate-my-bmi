import { Component } from '@angular/core';

@Component({
  selector: 'cmb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'calculate-my-bmi';
}
