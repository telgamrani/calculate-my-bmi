import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalculateMyBmiFormComponent } from './calculate-my-bmi-form/calculate-my-bmi-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IndicatorBarModule } from '../shared/components/indicator-bar/indicator-bar.module';

@NgModule({
  declarations: [CalculateMyBmiFormComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, IndicatorBarModule],
  exports: [CalculateMyBmiFormComponent],
})
export class CalculateMyBmiFormModule {}
