import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BmiInterpretation } from 'src/app/shared/enum/bmi-interpretation.enum';

@Component({
  selector: 'cmb-calculate-my-bmi-form',
  templateUrl: './calculate-my-bmi-form.component.html',
  styleUrls: ['./calculate-my-bmi-form.component.scss'],
})
export class CalculateMyBmiFormComponent {
  calculateBmiForm: FormGroup;
  isSubmitted = false;
  calculatedBmi: number;
  bmiInterpretation: BmiInterpretation;
  bmiInterpretationFrLabel: string;
  bmiInterpretationByWeightAndHeightFrLabel: string;
  normalWeightByHeight: { min: number; max: number };

  constructor(private fb: FormBuilder) {
    this.calculateBmiForm = this.createFormCalculateBmi();
  }

  calculateBmi(weight: number, height: number): number {
    const bmi = (weight / Math.pow(height / 100, 2)).toPrecision(3);
    return parseFloat(bmi);
  }

  private createFormCalculateBmi(): FormGroup {
    return this.fb.nonNullable.group(
      {
        weight: this.fb.control(0, [
          Validators.required,
          Validators.pattern('^[0-9.]*$'),
          Validators.min(1),
        ]),
        height: this.fb.control(0, [
          Validators.required,
          Validators.pattern('^[0-9.]*$'),
          Validators.min(1),
        ]),
      },
      { updateOn: 'submit' }
    );
  }

  submitCalculateBmiForm() {
    this.isSubmitted = true;
    if (this.calculateBmiForm.valid) {
      const weight = this.calculateBmiForm.controls['weight'].value;
      const height = this.calculateBmiForm.controls['height'].value;
      this.calculatedBmi = this.calculateBmi(weight, height);
      this.findBmiInterpretation(this.calculatedBmi);
      this.calculateNormalWeightByHeight(height);
    }
  }

  findBmiInterpretation(bmi: number) {
    if (bmi < 18.5) {
      this.bmiInterpretation = BmiInterpretation.Underweight;
      this.bmiInterpretationFrLabel = 'Poids insuffisant';
      this.bmiInterpretationByWeightAndHeightFrLabel =
        'votre poids est insuffisant';
    } else if (bmi >= 18.5 && bmi <= 24.9) {
      this.bmiInterpretation = BmiInterpretation.NormalWeight;
      this.bmiInterpretationFrLabel = 'Normal / Sain';
      this.bmiInterpretationByWeightAndHeightFrLabel = 'votre poids est normal';
    } else if (bmi >= 25 && bmi <= 29.9) {
      this.bmiInterpretation = BmiInterpretation.Overweight;
      this.bmiInterpretationFrLabel = 'Surpoids';
      this.bmiInterpretationByWeightAndHeightFrLabel = 'vous êtes en surpoids';
    } else if (bmi >= 30) {
      this.bmiInterpretation = BmiInterpretation.Obesity;
      this.bmiInterpretationFrLabel = 'Obésité';
      this.bmiInterpretationByWeightAndHeightFrLabel =
        'vous vous situez dans la catégorie des personnes obèses';
    }
  }

  calculateNormalWeightByHeight(height: number) {
    const minNormalBmi = 18.5;
    const maxNormalBmi = 24.9;
    this.normalWeightByHeight = {
      min: Math.ceil(minNormalBmi * Math.pow(height / 100, 2)),
      max: Math.ceil(maxNormalBmi * Math.pow(height / 100, 2)),
    };
  }
}
