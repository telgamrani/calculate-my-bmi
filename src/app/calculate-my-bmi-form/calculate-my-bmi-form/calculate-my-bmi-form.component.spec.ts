import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  AbstractControl,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { By } from '@angular/platform-browser';
import { BmiInterpretation } from 'src/app/shared/enum/bmi-interpretation.enum';

import { CalculateMyBmiFormComponent } from './calculate-my-bmi-form.component';

describe('CalculateMyBmiFormComponent', () => {
  let component: CalculateMyBmiFormComponent;
  let fixture: ComponentFixture<CalculateMyBmiFormComponent>;
  let debugElement: DebugElement;
  let weightControl: AbstractControl;
  let heightControl: AbstractControl;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CalculateMyBmiFormComponent],
      imports: [FormsModule, ReactiveFormsModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculateMyBmiFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
    weightControl = component.calculateBmiForm.controls['weight'];
    heightControl = component.calculateBmiForm.controls['height'];
  });

  it('create the component', () => {
    expect(component).toBeTruthy();
  });

  it('have a form to calculate bmi', () => {
    const weightInput = debugElement.query(
      By.css('input[data-test-weight-input]')
    );
    const heightInput = debugElement.query(
      By.css('input[data-test-height-input]')
    );
    const calculateBmiSubmitInput = debugElement.query(
      By.css('input[data-test-calculate-bmi-button]')
    );

    expect(weightInput).withContext('weightInput').toBeTruthy();
    expect(heightInput).withContext('heightInput').toBeTruthy();
    expect(calculateBmiSubmitInput)
      .withContext('calculateBmiSubmitInput')
      .toBeTruthy();
  });

  it('calculate bmi (to precisoin 3)', () => {
    let weight = 83;
    let height = 183;
    let bmi = 24.8; // 83 / (1.83)² = 24.78 to precision => 24.8
    let calculateBmi = component.calculateBmi(weight, height);
    expect(calculateBmi)
      .withContext('calculateBmi precision/round up')
      .toBe(bmi);

    weight = 66;
    height = 170;
    bmi = 22.8; // 66 / (1.70)² = 22.83 to precision => 22.8
    calculateBmi = component.calculateBmi(weight, height);
    expect(calculateBmi)
      .withContext('calculateBmi precision/round down')
      .toBe(bmi);
  });

  it('required input weight', () => {
    component.isSubmitted = true;
    weightControl.setValue(null);
    let weightErrors = weightControl.errors || {};
    expect(weightErrors['required']).withContext('invalid weight').toBeTruthy();

    fixture.detectChanges();
    expect(
      debugElement.query(By.css('[data-test-error-message-required-weight]'))
    )
      .withContext('exists error message required')
      .toBeTruthy();

    weightControl.setValue(80);
    weightErrors = weightControl.errors || {};
    expect(weightErrors['required']).withContext('valid weight').toBeFalsy();

    fixture.detectChanges();
    expect(
      debugElement.query(By.css('[data-test-error-message-required-weight]'))
    )
      .withContext("don't exist error message required")
      .toBeFalsy();
  });

  it('only numeric value input weight', () => {
    component.isSubmitted = true;
    weightControl.setValue('notNumber');
    let weightErrors = weightControl.errors || {};
    expect(weightErrors['pattern']).withContext('invalid weight').toBeTruthy();

    fixture.detectChanges();
    expect(
      debugElement.query(
        By.css('[data-test-error-message-only-numeric-weight]')
      )
    )
      .withContext('exists error message only numeric')
      .toBeTruthy();

    weightControl.setValue(80);
    weightErrors = weightControl.errors || {};
    expect(weightErrors['pattern']).withContext('valid weight').toBeFalsy();

    fixture.detectChanges();
    expect(
      debugElement.query(
        By.css('[data-test-error-message-only-numeric-weight]')
      )
    )
      .withContext('exists error message only numeric')
      .toBeFalsy();
  });

  it('input weight min 1', () => {
    component.isSubmitted = true;
    let weightErrors = weightControl.errors || {};
    expect(weightErrors['min']).withContext('invalid weight 0').toBeTruthy();

    fixture.detectChanges();
    expect(debugElement.query(By.css('[data-test-error-message-min-weight]')))
      .withContext('exists error message min')
      .toBeTruthy();

    weightControl.setValue(80);
    weightErrors = weightControl.errors || {};
    expect(weightErrors['min']).withContext('valid weight').toBeFalsy();

    fixture.detectChanges();
    expect(debugElement.query(By.css('[data-test-error-message-min-weight]')))
      .withContext('exists error message min')
      .toBeFalsy();
  });

  it('required input height', () => {
    component.isSubmitted = true;
    heightControl.setValue(null);
    let heightErrors = heightControl.errors || {};
    expect(heightErrors['required']).withContext('invalid height').toBeTruthy();

    fixture.detectChanges();
    expect(
      debugElement.query(By.css('[data-test-error-message-required-height]'))
    )
      .withContext('exists error required')
      .toBeTruthy();

    heightControl.setValue(80);
    heightErrors = heightControl.errors || {};
    expect(heightErrors['required']).withContext('valid height').toBeFalsy();

    fixture.detectChanges();
    expect(
      debugElement.query(By.css('[data-test-error-message-required-height]'))
    )
      .withContext('exists error required')
      .toBeFalsy();
  });

  it('only numeric value input height', () => {
    component.isSubmitted = true;
    heightControl.setValue('notNumber');
    let heightErrors = heightControl.errors || {};
    expect(heightErrors['pattern']).withContext('invalid height').toBeTruthy();

    fixture.detectChanges();
    expect(
      debugElement.query(
        By.css('[data-test-error-message-only-numeric-height]')
      )
    )
      .withContext('exists error message only numeric')
      .toBeTruthy();

    heightControl.setValue(80);
    heightErrors = heightControl.errors || {};
    expect(heightErrors['pattern']).withContext('valid height').toBeFalsy();

    fixture.detectChanges();
    expect(
      debugElement.query(
        By.css('[data-test-error-message-only-numeric-height]')
      )
    )
      .withContext('exists error message only numeric')
      .toBeFalsy();
  });

  it('input height min 1', () => {
    component.isSubmitted = true;
    let heightErrors = heightControl.errors || {};
    expect(heightErrors['min']).withContext('invalid height 0').toBeTruthy();

    fixture.detectChanges();
    expect(debugElement.query(By.css('[data-test-error-message-min-height]')))
      .withContext('exists error message min')
      .toBeTruthy();

    heightControl.setValue(80);
    heightErrors = heightControl.errors || {};
    expect(heightErrors['min']).withContext('valid height').toBeFalsy();

    fixture.detectChanges();
    expect(debugElement.query(By.css('[data-test-error-message-min-height]')))
      .withContext('exists error message min')
      .toBeFalsy();
  });

  it('submit valid calculate bmi form', () => {
    const weight = 83;
    const height = 183;
    weightControl.setValue(weight);
    heightControl.setValue(height);

    expect(component.isSubmitted).withContext('isSubmited False').toBeFalse();
    spyOn(component, 'calculateBmi');
    spyOn(component, 'findBmiInterpretation');
    component.submitCalculateBmiForm();

    expect(component.calculateBmi)
      .withContext('called once calculateBmi function')
      .toHaveBeenCalledOnceWith(weight, height);

    expect(component.findBmiInterpretation)
      .withContext('called once findBmiInterpretation function')
      .toHaveBeenCalledTimes(1);

    expect(component.isSubmitted).withContext('isSubmitted true').toBeTrue();
  });

  it('does not submit invalid calculate bmi form', () => {
    const weight = -1;
    const height = -1;
    weightControl.setValue(weight);
    heightControl.setValue(height);

    spyOn(component, 'calculateBmi');
    component.submitCalculateBmiForm();
    expect(component.calculateBmi)
      .withContext('called once calculateBmi function')
      .not.toHaveBeenCalledOnceWith(weight, height);
  });

  it('dispaly bloc bmi result when valid form submitted', () => {
    const weight = 83;
    const height = 183;
    weightControl.setValue(weight);
    heightControl.setValue(height);

    component.submitCalculateBmiForm();
    fixture.detectChanges();

    expect(debugElement.query(By.css('[data-test-bmi-result-bloc]')))
      .withContext('exists bmi result bloc')
      .toBeTruthy();
  });

  it('find the bmi interpretaion by calculated bmi', () => {
    let bmi = 15;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 15')
      .toBe(BmiInterpretation.Underweight);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();

    component.bmiInterpretationFrLabel = '';
    component.bmiInterpretationByWeightAndHeightFrLabel = '';
    bmi = 18.4;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 18.4')
      .toBe(BmiInterpretation.Underweight);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();

    component.bmiInterpretationFrLabel = '';
    component.bmiInterpretationByWeightAndHeightFrLabel = '';
    bmi = 18.5;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 18.5')
      .toBe(BmiInterpretation.NormalWeight);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();

    component.bmiInterpretationFrLabel = '';
    component.bmiInterpretationByWeightAndHeightFrLabel = '';
    bmi = 20;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 20')
      .toBe(BmiInterpretation.NormalWeight);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();

    component.bmiInterpretationFrLabel = '';
    component.bmiInterpretationByWeightAndHeightFrLabel = '';
    bmi = 24.9;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 24.9')
      .toBe(BmiInterpretation.NormalWeight);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();

    component.bmiInterpretationFrLabel = '';
    component.bmiInterpretationByWeightAndHeightFrLabel = '';
    bmi = 25;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 25')
      .toBe(BmiInterpretation.Overweight);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();

    component.bmiInterpretationFrLabel = '';
    component.bmiInterpretationByWeightAndHeightFrLabel = '';
    bmi = 27;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 24')
      .toBe(BmiInterpretation.Overweight);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();

    component.bmiInterpretationFrLabel = '';
    component.bmiInterpretationByWeightAndHeightFrLabel = '';
    bmi = 29.9;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 29.9')
      .toBe(BmiInterpretation.Overweight);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();

    component.bmiInterpretationFrLabel = '';
    component.bmiInterpretationByWeightAndHeightFrLabel = '';
    bmi = 30;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 30')
      .toBe(BmiInterpretation.Obesity);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();

    component.bmiInterpretationFrLabel = '';
    component.bmiInterpretationByWeightAndHeightFrLabel = '';
    bmi = 40;
    component.findBmiInterpretation(bmi);
    expect(component.bmiInterpretation)
      .withContext('Underweight bmi 40')
      .toBe(BmiInterpretation.Obesity);
    expect(component.bmiInterpretationFrLabel)
      .withContext('bmiInterpretationFrLabel')
      .toBeTruthy();
    expect(component.bmiInterpretationByWeightAndHeightFrLabel)
      .withContext('bmiInterpretationByWeightAndHeightFrLabel')
      .toBeTruthy();
  });

  it('display bloc bmi intrepretation when form is valid', () => {
    const weight = 83;
    const height = 183;
    weightControl.setValue(weight);
    heightControl.setValue(height);

    component.submitCalculateBmiForm();
    fixture.detectChanges();

    expect(debugElement.query(By.css('[data-test-bmi-interpretation-bloc]')))
      .withContext('exists bmi interpretation bloc')
      .toBeTruthy();
  });

  it('display bloc bmi intrepretation by weight and height when form is valid', () => {
    const weight = 83;
    const height = 183;
    weightControl.setValue(weight);
    heightControl.setValue(height);

    component.submitCalculateBmiForm();
    fixture.detectChanges();

    expect(
      debugElement.query(
        By.css('[data-test-bmi-interpretation-by-weight-and-height-bloc]')
      )
    )
      .withContext('exists bmi interpretation by weight and height bloc')
      .toBeTruthy();
  });

  it('calculate the normal weight for a given height', () => {
    const height = 183;
    const normalWeightByHeight = { min: 62, max: 84 };

    component.calculateNormalWeightByHeight(height);
    expect(component.normalWeightByHeight)
      .withContext('normalWeightByHeight')
      .toEqual(normalWeightByHeight);
  });

  it('display bloc normal min and max weight for a given height', () => {
    const height = 183;
    const weight = 83;
    weightControl.setValue(weight);
    heightControl.setValue(height);

    component.submitCalculateBmiForm();
    fixture.detectChanges();

    expect(
      debugElement.query(
        By.css(
          '[data-test-bmi-normal-by-min-and-max-weight-by-given-height-bloc]'
        )
      )
    )
      .withContext(
        'exists bmi normal by min and max weight by given height bloc'
      )
      .toBeTruthy();
  });
});
