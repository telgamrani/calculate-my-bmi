import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListBlogPostsPageComponent } from './list-blog-posts-page/list-blog-posts-page.component';
import { RouterModule } from '@angular/router';
import { ReplaceSpacesWithModule } from '../shared/pipes/replace-spaces-with/replace-spaces-with.module';

@NgModule({
  declarations: [ListBlogPostsPageComponent],
  imports: [CommonModule, RouterModule, ReplaceSpacesWithModule],
  exports: [ListBlogPostsPageComponent],
})
export class ListBlogPostsPageModule {}
