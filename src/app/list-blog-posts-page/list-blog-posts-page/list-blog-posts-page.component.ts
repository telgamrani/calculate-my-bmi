import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/core/services/post.service';
import { Post } from 'src/app/shared/models/post.model';

@Component({
  selector: 'cmb-list-blog-posts-page',
  templateUrl: './list-blog-posts-page.component.html',
  styleUrls: ['./list-blog-posts-page.component.scss'],
})
export class ListBlogPostsPageComponent implements OnInit {
  posts: Array<Post>;

  constructor(private postService: PostService) {}

  ngOnInit(): void {
    this.fetchAllPosts();
  }

  fetchAllPosts() {
    this.postService.fetchAllPosts().subscribe((posts: Array<Post>) => {
      this.posts = posts;
    });
  }
}
