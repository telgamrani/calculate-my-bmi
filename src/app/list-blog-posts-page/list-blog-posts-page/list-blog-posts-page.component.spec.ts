import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { By } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

import { ListBlogPostsPageComponent } from './list-blog-posts-page.component';

describe('ListBlogPostsPageComponent', () => {
  let component: ListBlogPostsPageComponent;
  let fixture: ComponentFixture<ListBlogPostsPageComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListBlogPostsPageComponent],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ListBlogPostsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement;
  });

  it('create component', () => {
    expect(component).toBeTruthy();
  });

  xit('have article html element', () => {
    expect(debugElement.query(By.css('article')))
      .withContext('article element')
      .toBeTruthy();
  });

  xit('have h1 html element', () => {
    expect(debugElement.query(By.css('h1')))
      .withContext('h1 element')
      .toBeTruthy();
  });

  xit('have h2 html element', () => {
    expect(debugElement.query(By.css('h2')))
      .withContext('h2 element')
      .toBeTruthy();
  });
});
