import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HeaderModule } from './shared/components/header/header.module';
import { HomePageModule } from './home-page/home-page.module';
import { FooterModule } from './shared/components/footer/footer.module';
import { BlogPostPageModule } from './blog-post-page/blog-post-page.module';
import { ListBlogPostsPageModule } from './list-blog-posts-page/list-blog-posts-page.module';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { environment } from 'src/environments/environment';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page/home-page.component';
import { BlogPostPageComponent } from './blog-post-page/blog-post-page/blog-post-page.component';
import { ListBlogPostsPageComponent } from './list-blog-posts-page/list-blog-posts-page/list-blog-posts-page.component';

const routes: Routes = [
  { path: 'calcul-imc', component: HomePageComponent },
  { path: 'articles', component: ListBlogPostsPageComponent },
  {
    path: 'articles',
    children: [
      {
        path: ':id',
        component: BlogPostPageComponent,
      },
    ],
  },
  { path: '**', component: HomePageComponent },
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HeaderModule,
    FooterModule,
    HomePageModule,
    BlogPostPageModule,
    ListBlogPostsPageModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
